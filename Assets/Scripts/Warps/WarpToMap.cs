﻿using UnityEngine;
using System.Collections;

public class WarpToMap : MonoBehaviour {

	public int levelToLoad = 0;
	public int levelToRequest = -1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col)
	{
		string tag = col.gameObject.tag;
		if(tag.Equals ("Player"))
		{
			Screen.lockCursor = false;
			RequestedLevel.RequestLevel (levelToRequest);
			Application.LoadLevel (levelToLoad);
		}
	}
}
