﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(MeshRenderer))]
public class WannabeShimmer : MonoBehaviour {

	public float minAngle = 0.15f;
	public float maxAngle = 0.50f;
	public float step = 0.01f;

	private float curAngle;
	private float dir;

	// Use this for initialization
	void Start () {
		curAngle = minAngle;
		dir = 1;
	}
	
	// Update is called once per frame
	void Update () {
		if(curAngle >= maxAngle || curAngle <= minAngle)
		{
			dir *= -1;
		}
		curAngle += dir * step * Time.deltaTime;
		curAngle = Mathf.Clamp (curAngle, minAngle, maxAngle);

		renderer.material.SetFloat ("_Angle", curAngle);
	}
}
