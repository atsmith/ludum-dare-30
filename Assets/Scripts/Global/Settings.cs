﻿using UnityEngine;
using System.Collections;

public static class Settings
{
	public static float ROUNDING_ERROR { get { return 0.000001f; } }

	private static bool invert = false;

	public static bool isInverted { get { return invert; } }

	public static void SetInverted(bool inv)
	{
		invert = inv;
	}

	public static void LoadSettings()
	{
		invert = ((PlayerPrefs.GetInt ("invert", 0) != 0)? true : false);
	}

	public static void SaveSettings()
	{
		PlayerPrefs.SetInt ("invert", ((isInverted)?1:0));

		PlayerPrefs.Save();
	}
}
