﻿using UnityEngine;
using System.Collections;

public static class GameState {

	private static bool paused = false;

	public static bool isPaused { get { return paused; } }

	public static void TogglePause()
	{
		paused = !paused;
	}
}
