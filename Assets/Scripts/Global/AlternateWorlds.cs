﻿using UnityEngine;
using System.Collections;

public static class AlternateWorlds
{

	public enum CurrentWorld
	{
		BLACK,
		WHITE
	}

	private static CurrentWorld selectedWorld = CurrentWorld.BLACK;

	public static void SetWorldType(CurrentWorld type)
	{
		selectedWorld = type;
	}

	public static CurrentWorld worldType { get { return selectedWorld; } }
}
