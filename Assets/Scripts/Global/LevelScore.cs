﻿using UnityEngine;
using System;
using System.Collections;

public static class LevelScore
{
	private static int totalDeaths = 0;
	private static float timeTaken = 0.0f;
	private static int currentLevel;
	private static int jumpCount = 0;

	public static int deathCount { get { return totalDeaths; } }
	public static float levelTime { get { return timeTaken; } }
	public static int level { get { return currentLevel; } }
	public static int jumps { get { return jumpCount; } }

	public static void DeathPenalty()
	{
		totalDeaths += 1;
	}

	public static void tick()
	{
		if(GameState.isPaused) return;
		timeTaken += Time.deltaTime;
	}

	public static void SetLevel(int lev)
	{
		currentLevel = lev;
	}

	public static void jumped()
	{
		jumpCount += 1;
	}

	public static void LevelBeaten()
	{
		Score.SetHighScore (new HighScore(level, levelTime, deathCount));
		totalDeaths = 0;
		timeTaken = 0;
		jumpCount = 0;
	}
}
