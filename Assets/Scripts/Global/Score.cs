﻿using UnityEngine;
using System;
using System.Collections;

public static class Score
{

	public static HighScore GetHighScore(int level)
	{
		string format = level + "_score.";
		float time = PlayerPrefs.GetFloat (format + "time", -1);
		int deaths = PlayerPrefs.GetInt (format + "deaths", -1);
		return new HighScore(level, time, deaths);
	}

	public static bool HasHighScore(int level)
	{
		string format = level + "_score.";
		if(PlayerPrefs.GetFloat (format + "time", -1) == -1 || PlayerPrefs.GetInt (format + "deaths", -1) == -1)
		{
			return false;
		}
		return true;
	}

	public static bool SetHighScore(HighScore score)
	{
		bool high = false;
		if(HasHighScore (score.level))
		{
			if(score < GetHighScore (score.level))
			{
				high = true;
			}
		}
		else
		{
			high = true;
		}

		if(high)
		{
			PlayerPrefs.SetFloat (score.level + "_score.time", (float)score.time);
			PlayerPrefs.SetInt (score.level + "_score.deaths", score.deaths);
		}

		return high;
	}

}
