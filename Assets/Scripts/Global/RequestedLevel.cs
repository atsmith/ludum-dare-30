﻿using UnityEngine;
using System.Collections;

public static class RequestedLevel {

	private static int levelRequested = -1;

	public static int requestedLevel { get { return levelRequested; } }

	public static void RequestLevel(int level)
	{
		levelRequested = level;
	}
	
}
