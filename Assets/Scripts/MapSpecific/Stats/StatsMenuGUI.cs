﻿using UnityEngine;
using System.Collections;

public class StatsMenuGUI : MonoBehaviour {

	[System.Serializable]
	public struct levelInfo
	{
		public int levelID;
		public string levelName;
	};
	public Rect guiStuff = new Rect(0.1f, 0.1f, 0.8f, 0.8f);
	public levelInfo[] levels = null;

	private string selectedName = string.Empty;
	private string selectedStats = string.Empty;
	private int selected = 0;
	private Rect guiArea;
	private Rect guiBack;
	private GUIContent guiBackText;
	private int width, height;
	private Vector2 scrollPosition = Vector2.zero;
	private int oldSelected=0;

	// Use this for initialization
	void Start () {
		guiStuff.x = Mathf.Clamp (guiStuff.x, 0, 1);
		guiStuff.y = Mathf.Clamp (guiStuff.y, 0, 1);
		guiStuff.width = Mathf.Clamp (guiStuff.width, 0, 1);
		guiStuff.height = Mathf.Clamp (guiStuff.height, 0, 1);

		SetupMenu ();
		GetStats ();

		guiBackText = new GUIContent("Back (Esc)");
	}

	void SetupMenu()
	{
		width = Screen.width;
		height = Screen.height;
		guiArea = new Rect(guiStuff.x * width, guiStuff.y * height,
		                   guiStuff.width * width, guiStuff.height * height);

		guiBack = new Rect(0, guiArea.height + guiArea.y + 0.01f * height,
		                   0,0);
	}
	
	// Update is called once per frame
	void Update () {
		if(Screen.width != width || Screen.height != height)
		{
			SetupMenu ();
		}

		if(selected != oldSelected)
		{
			GetStats ();
			oldSelected = selected;
		}

		if(Input.GetKeyDown (KeyCode.Escape))
		{
			Application.LoadLevel (1);
		}
	}

	void GetStats()
	{
		if(0 == selected)
		{
			selectedName = "Stat Totals";
			selectedStats = "Total Plays: " + PlayerPrefs.GetInt ("tally.plays", 0) + "\n\n";
			selectedStats+= "Total Time: " + PlayerPrefs.GetFloat ("tally.time", 0) + " seconds\n\n";
			selectedStats+= "Total Deaths: "+ PlayerPrefs.GetInt ("tally.deaths", 0) + "\n\n";
			selectedStats+= "Total Jumps: " + PlayerPrefs.GetInt ("tally.jumps", 0) + "\n\n";
			return;
		}
		selectedName = levels[selected-1].levelName;
		selectedStats = "Plays: " + PlayerPrefs.GetInt (levels[selected-1].levelID + "_tally.plays", 0) + "\n\n";
		selectedStats+= "Time: " + PlayerPrefs.GetFloat (levels[selected-1].levelID + "_tally.time", 0) + " seconds\n\n";
		selectedStats+= "Deaths: " + PlayerPrefs.GetInt (levels[selected-1].levelID + "_tally.deaths", 0) + "\n\n";
		selectedStats+= "Jumps: " + PlayerPrefs.GetInt (levels[selected-1].levelID + "_tally.jumps", 0) + "\n\n";
	}

	void OnGUI()
	{
		GUI.Box (guiArea, "");
		GUILayout.BeginArea (guiArea);
		{
			GUILayout.BeginHorizontal ();
			{
				scrollPosition = GUILayout.BeginScrollView (scrollPosition, GUILayout.MaxWidth (120));
				{
					GUILayout.BeginVertical ();
					{
						if(GUILayout.Button ("Total Stats", GUILayout.Height (30)))
						{
							selected = 0;
						}
						int i = 1;
						foreach(levelInfo level in levels)
						{
							if(GUILayout.Button (level.levelName, GUILayout.Height (30)))
							{
								selected = i;
							}
							++i;
						}
					}
					GUILayout.EndVertical ();
				}
				GUILayout.EndScrollView ();
				GUILayout.BeginVertical ();
				{
					int oldSize = GUI.skin.label.fontSize;
					GUI.skin.label.fontSize = 26;
					Vector2 oldBat = GUI.skin.label.CalcSize (new GUIContent(selectedName));
					GUILayout.Label (selectedName, GUILayout.MinHeight (oldBat.y));
					GUI.skin.label.fontSize = oldSize;
					GUILayout.Label (selectedStats, GUILayout.ExpandHeight (true), GUILayout.ExpandWidth (true));

				}
				GUILayout.EndVertical ();
			}
			GUILayout.EndHorizontal ();
		}
		GUILayout.EndArea ();

		Vector2 oldGuy = GUI.skin.button.CalcSize (guiBackText);
		guiBack.height = 2.0f * oldGuy.y;
		guiBack.width = 1.2f * oldGuy.x;
		guiBack.x = 0.5f * width - guiBack.width;
		if(GUI.Button (guiBack, guiBackText))
		{
			Application.LoadLevel (1);
		}
	}
}
