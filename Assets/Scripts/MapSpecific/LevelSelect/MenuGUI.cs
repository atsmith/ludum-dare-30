﻿using UnityEngine;
using System.Collections;

public class MenuGUI : MonoBehaviour {

	public Vector2 guiLocation;
	public Vector2 guiSize;

	[System.Serializable]
	public struct levelInfo
	{
		public int level;
		public string name;
		public string desc;
	};

	public levelInfo[] levels = new levelInfo[1];

	private Rect guiArea;
	private Rect backButton;
	private int width, height;

	private GUIContent backText;

	private Vector2 scrollPosition = Vector2.zero;

	// Use this for initialization
	void Start () {
		guiLocation.x = Mathf.Clamp (guiLocation.x, 0, 1);
		guiLocation.y = Mathf.Clamp (guiLocation.y, 0, 1);

		guiSize.x = Mathf.Clamp (guiSize.x, 0, 1);
		guiSize.y = Mathf.Clamp (guiSize.y, 0, 1);

		backText = new GUIContent("Back (Esc)");

		SetupMenu ();
	}

	void SetupMenu()
	{
		width = Screen.width;
		height = Screen.height;
		guiArea = new Rect(guiLocation.x * width, guiLocation.y * height,
		                   guiSize.x * width, guiSize.y * height);
		backButton = new Rect(0.5f * width, guiArea.height + guiArea.y + 0.01f * height,
		                      guiSize.x * width / 4, 30);
	}
	
	// Update is called once per frame
	void Update () {
		Screen.lockCursor = false;

		if(Screen.height != height || Screen.width != width)
		{
			SetupMenu();
		}

		if(Input.GetKeyDown (KeyCode.Escape))
		{
			Application.LoadLevel (1);
		}
	}

	string GetStats(int levelID)
	{
		string s = 	"Plays: " + PlayerPrefs.GetInt (levelID + "_tally.plays", 0) + " ";
		s +=		"Deaths: " + PlayerPrefs.GetInt (levelID + "_tally.deaths", 0) + " ";
		s +=		"Time: " + PlayerPrefs.GetFloat (levelID + "_tally.time", 0);
		return s;
	}

	void OnGUI()
	{
		GUI.Box (guiArea, "");
		GUILayout.BeginArea (guiArea);
		{
			scrollPosition = GUILayout.BeginScrollView (scrollPosition, GUILayout.MinHeight (guiArea.height));
			{
				GUILayout.BeginVertical (GUILayout.ExpandHeight (true));
				{
					foreach(levelInfo level in levels)
					{
						GUILayout.BeginHorizontal ();
						{
							GUILayout.BeginVertical ();
							{
								int oldSize = GUI.skin.label.fontSize;
								GUI.skin.label.fontSize = 18;
								GUIContent pope = new GUIContent(level.name + "");
								Vector2 bob = GUI.skin.label.CalcSize (pope);
								GUILayout.Label (level.name, GUILayout.MinHeight (bob.y));
								GUI.skin.label.fontSize = oldSize;
								GUILayout.Label (GetStats (level.level));
								GUILayout.Label (level.desc);
							}
							GUILayout.EndVertical ();

							if(GUILayout.Button ("Start " + level.name, GUILayout.ExpandWidth(false)))
							{
								RequestedLevel.RequestLevel (level.level);
								Application.LoadLevel (13);
							}
						}
						GUILayout.EndHorizontal ();
					}
				}
				GUILayout.EndVertical ();
			}
			GUILayout.EndScrollView ();
		}
		GUILayout.EndArea ();
		Vector2 ape = GUI.skin.button.CalcSize (backText);
		backButton.height = 1.2f * ape.y;
		backButton.width = 2.0f * ape.x;
		backButton.x = 0.5f * width - 0.5f * backButton.width;
		if(GUI.Button (backButton, backText))
		{
			Application.LoadLevel (1);
		}
	}
}
