﻿using UnityEngine;
using System.Collections;

public class BigMenu : MonoBehaviour {

	private float menuWidth;
	private float menuHeight;

	private Rect menuLocation;
	private float width, height;

	private Rect backButton;

	private bool toggle = Settings.isInverted;

	// Use this for initialization
	void Start () {
		width = (float)Screen.width;
		height = (float)Screen.height;
		SetupMenu();
	}

	void SetupMenu()
	{
		menuWidth = 0.8f * width;
		menuHeight = 0.75f * height;
		menuLocation = new Rect(0.1f * width, 0.1f * height, menuWidth, menuHeight);
		backButton = new Rect(0.1f * width, 0.85f * height, 150.0f, 0.1f * height);
	}
	
	// Update is called once per frame
	void Update () {
		bool readjust = false;
		if(Screen.width != width)
		{
			width = Screen.width;
			readjust = true;
		}
		if(Screen.height != height)
		{
			height = Screen.height;
			readjust = true;
		}
		if(readjust)
		{
			SetupMenu ();
		}
		if(Input.GetKeyDown (KeyCode.Escape))
		{
			Application.LoadLevel (1);
		}
	}

	void OnGUI()
	{
		GUI.Box (menuLocation, "");
		GUI.Box (menuLocation, "");
		GUI.Box (menuLocation, "");
		GUILayout.BeginArea (menuLocation);
		{
			GUILayout.BeginVertical (GUILayout.MinWidth (menuWidth), GUILayout.MaxWidth (menuWidth));
			{

				bool update = GUILayout.Toggle (toggle, "Invert Y-Axis?");
				if(update != toggle)
				{
					Settings.SetInverted(update);
					toggle = update;
				}
				
			}
			GUILayout.EndVertical ();
		}
		GUILayout.EndArea ();

		if(GUI.Button (backButton, "Back"))
		{
			Settings.SaveSettings();
			Application.LoadLevel (1);
		}
	}
}
