﻿using UnityEngine;
using System.Collections;

public class Logo : MonoBehaviour {

	public float alphaSpeed = 5;

	private float time;
	private bool good;
	private GUIText te;
	private Color alpha;

	// Use this for initialization
	void Start () {
		time = 2.0f;
		good = false;
		te = null;
	}
	
	// Update is called once per frame
	void Update () {
		if(good)
		{
			time -= Time.deltaTime;
			if(time <= 0)
			{
				Application.LoadLevel (16);
			}
		}
		if(null != te && !(te.material.color.a >= 1))
		{
			alpha.a += alphaSpeed * Time.deltaTime;
			te.material.color = alpha;
		}
		else if(null != te)
		{
			good = true;
		}
	}

	void DisplayMessage()
	{
		GameObject obj = new GameObject("SecondaryMessage");
		te = obj.AddComponent<GUIText>();
		te.text = "What it's really doing is loading settings.";
		te.anchor = TextAnchor.MiddleCenter;
		te.alignment = TextAlignment.Center;
		te.fontSize = 18;
		obj.transform.position = new Vector3(0.5f, 0.3f, 0);
		alpha = te.material.color;
		alpha.a = 0;
		te.material.color = alpha;
	}
}
