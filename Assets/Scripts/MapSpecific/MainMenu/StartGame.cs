﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour {

	public GUIText gameTitle = null;
	public float someStep = 0.1f;

	private bool fadeout = false;
	private Color col;
	private GUIText master;
	// Use this for initialization
	void Start () {
		master = GetComponent<GUIText>();
		col = master.material.color;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.anyKey)
		{
			if(null != gameTitle)
			{
				Animator an = gameTitle.transform.gameObject.GetComponent<Animator>();
				an.enabled = true;
				an.Play ("MoveToTop");
			}

			fadeout=true;
		}
		if(fadeout)
		{
			col.a -= someStep;
			master.material.color = col;
		}
		if(col.a <= 0)
		{
			Destroy (transform.gameObject);
		}
	}
}
