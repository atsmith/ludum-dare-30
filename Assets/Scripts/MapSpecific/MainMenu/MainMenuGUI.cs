﻿using UnityEngine;
using System.Collections;

public class MainMenuGUI : MonoBehaviour {

	public float menuWidth;
	public float menuHeight;
	public float buttonHeight;

	private Rect group;

	private bool guiEnabled = true;

	private int width, height;

	// Use this for initialization
	void Start () {
		width = Screen.width;
		height = Screen.height;
		SetupMenu ();
	}

	void SetupMenu()
	{
		group = new Rect(width * 0.5f - menuWidth * 0.5f,
		                 height * 0.4f,
		                 menuWidth, menuHeight);
	}
	
	// Update is called once per frame
	void Update () {
		bool readjust = false;
		if(Screen.width != width)
		{
			width = Screen.width;
			readjust = true;
		}
		if(Screen.height != height)
		{
			height = Screen.height;
			readjust = true;
		}
		if(readjust)
		{
			SetupMenu ();
		}
	}

	void IsQuitting(bool response)
	{
		if(response)
		{
			Application.Quit ();
		}
		guiEnabled = true;
	}

	void OnGUI()
	{
		GUI.enabled = guiEnabled;
		GUILayout.BeginArea (group);
		GUILayout.BeginVertical ();
		if(GUILayout.Button ("Play", GUILayout.Height (buttonHeight)))
		{
			Application.LoadLevel (2);
		}
		if(GUILayout.Button ("Tutorials", GUILayout.Height (buttonHeight)))
		{
			Application.LoadLevel (3);
		}
		if(GUILayout.Button ("Stats", GUILayout.Height (buttonHeight)))
		{
			Application.LoadLevel (4);
		}
		if(GUILayout.Button ("Settings", GUILayout.Height (buttonHeight)))
		{
			Application.LoadLevel (5);
		}
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_STANDALONE_LINUX || UNITY_STANDALONE || UNITY_EDITOR
		if(GUILayout.Button ("Quit", GUILayout.Height (buttonHeight)))
		{
			guiEnabled = false;
			YesNoBox.CreateBox (IsQuitting, "You sure?", "Are you sure you want to quit? We will miss you a lot!", new Vector2(200, 100));
		}
#endif
		GUILayout.EndVertical ();
		GUILayout.EndArea ();
		GUI.enabled = true;
	}
}
