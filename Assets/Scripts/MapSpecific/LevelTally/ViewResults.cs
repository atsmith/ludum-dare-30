﻿using UnityEngine;
using System;
using System.Collections;

public class ViewResults : MonoBehaviour {

	private int width, height;
	private Rect guiArea;

	int deaths;
	float time;
	int jumpCount;

	// Use this for initialization
	void Start () {
		deaths = LevelScore.deathCount;
		time = LevelScore.levelTime;
		jumpCount = LevelScore.jumps;



		LevelScore.LevelBeaten ();




		SetupMenu();
	}

	void SetupMenu ()
	{
		width = Screen.width;
		height = Screen.height;
		guiArea = new Rect(0.05f * width, 0.2f * height, 0.9f * width, 0.6f * height);
	}

	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		GUI.Box (guiArea, "");
		GUI.Box (guiArea, "");
		GUI.Box (guiArea, "");
		GUILayout.BeginArea (guiArea);
		{
			GUILayout.BeginVertical ();
			{
				GUILayout.BeginHorizontal ();
				{
					GUILayout.Label ("Time: ", GUILayout.MaxWidth (0.3f * width));
					GUILayout.Label (time.ToString ());
				}
				GUILayout.EndHorizontal ();

				GUILayout.BeginHorizontal ();
				{
					GUILayout.Label ("Deaths: ", GUILayout.MaxWidth (0.3f * width));
					GUILayout.Label (deaths.ToString ());
				}
				GUILayout.EndHorizontal ();

				GUILayout.BeginHorizontal ();
				{
					GUILayout.Label ("Total Jumps: ", GUILayout.MaxWidth (0.3f * width));
					GUILayout.Label (jumpCount.ToString ());
				}
				GUILayout.EndHorizontal ();
				GUILayout.Space (0.15f * height);
				int oldFont = GUI.skin.label.fontSize;
				GUI.skin.label.fontSize = 26;
				GUILayout.Label ("Room for more stats!!!", GUILayout.ExpandHeight(true));
				GUI.skin.label.fontSize = oldFont;
			}
			GUILayout.EndVertical ();
			GUILayout.BeginHorizontal ();
			{
				if(GUILayout.Button ("Main Menu", GUILayout.MaxWidth (0.3f * width)))
				{
					RequestedLevel.RequestLevel(-1);
					Application.LoadLevel (1);
				}
				GUILayout.Space (0.3f * width);
				if(GUILayout.Button ("Continue", GUILayout.MaxWidth (0.3f * width)))
				{
					if(RequestedLevel.requestedLevel < 0)
					{
						Application.LoadLevel (2);
					}
					else if(RequestedLevel.requestedLevel == 3)
					{
						RequestedLevel.RequestLevel (-1);
						Application.LoadLevel (3);
					}
					Application.LoadLevel (13);
				}
			}
			GUILayout.EndHorizontal ();
		}
		GUILayout.EndArea ();
	}
}
