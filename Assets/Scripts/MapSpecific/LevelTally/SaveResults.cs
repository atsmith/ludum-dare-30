﻿using UnityEngine;
using System.Collections;

public class SaveResults : MonoBehaviour {

	// Use this for initialization
	void Start () {
		int timesPlayed = PlayerPrefs.GetInt (LevelScore.level + "_tally.plays", 0);
		float totalTime = PlayerPrefs.GetFloat (LevelScore.level + "_tally.time", 0);
		int deaths = PlayerPrefs.GetInt (LevelScore.level + "_tally.deaths", 0);
		int jumps = PlayerPrefs.GetInt (LevelScore.level + "_tally.jumps", 0);
		float totalTimePlayed = PlayerPrefs.GetFloat ("tally.time", 0);
		int totalDeaths = PlayerPrefs.GetInt ("tally.deaths", 0);
		int totalJumps = PlayerPrefs.GetInt ("tally.jumps", 0);
		int totalPlays = PlayerPrefs.GetInt ("tally.plays", 0);

		timesPlayed++;
		totalPlays++;
		totalTime += LevelScore.levelTime;
		totalTimePlayed += LevelScore.levelTime;
		deaths += LevelScore.deathCount;
		totalDeaths += LevelScore.deathCount;
		jumps += LevelScore.jumps;
		totalJumps += LevelScore.jumps;

		totalTime = ((float)Mathf.RoundToInt (totalTime * 1000)) / 1000.0f;
		totalTimePlayed = ((float)Mathf.RoundToInt (totalTime * 1000)) / 1000.0f;

		PlayerPrefs.SetInt (LevelScore.level + "_tally.plays", timesPlayed);
		PlayerPrefs.SetFloat (LevelScore.level + "_tally.time", totalTime);
		PlayerPrefs.SetInt (LevelScore.level + "_tally.deaths", deaths);
		PlayerPrefs.SetInt (LevelScore.level + "_tally.jumps", jumps);
		PlayerPrefs.SetFloat ("tally.time", totalTimePlayed);
		PlayerPrefs.SetInt ("tally.deaths", totalDeaths);
		PlayerPrefs.SetInt ("tally.jumps", totalJumps);
		PlayerPrefs.SetInt ("tally.plays", totalPlays);

		PlayerPrefs.Save ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
