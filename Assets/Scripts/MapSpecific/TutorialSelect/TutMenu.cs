﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TutMenu : MonoBehaviour {

	[Serializable]
	public struct stageInfo
	{
		public Texture2D image;
		public int level;
		public int reqLevel;
		public string desc;

		public float ratio()
		{
			return image.height / image.width;
		}
	};

	public stageInfo[] stages = new stageInfo[6];

	private int selected = 0;
	private int size;

	private Rect menuArea;
	private float width, height;

	private string stats;
	private bool readjust;

	// Use this for initialization
	void Start () {
		size = stages.Length;
		SetupMenu ();
		GetStats ();
		readjust = false;
	}

	void SetupMenu()
	{
		width = Screen.width;
		height = Screen.height;
		menuArea = new Rect(0.05f * width, 0.2f * height, 0.9f * width, 0.6f * height);
	}
	
	// Update is called once per frame
	void Update () {
		if(Screen.height != height || Screen.width != width)
		{
			SetupMenu ();
		}
		if(Input.GetButtonDown ("strafe"))
		{
			readjust = true;
			float ax = Input.GetAxis ("strafe");
			if(ax < 0)
			{
				selected--;
			}
			else
			{
				selected++;
			}
			if(selected < 0)
			{
				selected = size-1;
			}
			if(selected >= size)
			{
				selected = 0;
			}

		}

		if(readjust)
		{
			GetStats();
			readjust = false;
		}
	}

	void GetStats()
	{
		int lvl = stages[selected].reqLevel;
		int deaths = PlayerPrefs.GetInt (lvl + "_tally.deaths", 0);
		int plays = PlayerPrefs.GetInt (lvl + "_tally.plays", 0);
		float time = PlayerPrefs.GetFloat (lvl + "_tally.time", 0);
		int jumps = PlayerPrefs.GetInt (lvl + "_tally.jumps", 0);
		stats = "Statistics\n\n";
		stats += "Plays: " + plays + "\n";
		stats += "Time: " + time + " Seconds\n";
		stats += "Deaths: " + deaths + "\n";
		stats += "Jumps: " + jumps + "\n";
	}

	void OnGUI()
	{
		bool old = GUI.skin.box.wordWrap;
		GUI.skin.box.wordWrap = true;
		GUILayout.BeginArea (menuArea);
		{
			GUILayout.BeginVertical();
			{
				GUILayout.BeginHorizontal ();
				{
					GUILayout.Box ("Description\n\n" + stages[selected].desc, GUILayout.ExpandHeight(true), GUILayout.MinWidth (0.14f * width));
					GUILayout.BeginVertical ();
					{
						GUILayout.Box (stages[selected].image, GUILayout.MaxWidth (0.6f * width), GUILayout.MinWidth(0.6f * width));
						GUILayout.BeginHorizontal ();
						{
							if(GUILayout.Button ("Previous", GUILayout.MaxWidth (0.25f * width)))
							{
								readjust = true;
								selected--;
								if(selected < 0)
								{
									selected = size - 1;
								}
							}
							GUILayout.Space (0.1f * width);
							if(GUILayout.Button ("Next", GUILayout.MaxWidth (0.25f * width)))
							{
								readjust = true;
								selected++;
								if(selected >= size)
								{
									selected = 0;
								}
							}
						}
						GUILayout.EndHorizontal();
					}
					GUILayout.EndVertical ();
					GUILayout.Space(0.02f * width * -1);
					GUILayout.Box (stats, GUILayout.ExpandHeight(true), GUILayout.MinWidth (0.14f * width));
				}
				GUILayout.EndHorizontal ();
				GUILayout.BeginHorizontal ();
				{
					if(GUILayout.Button ("Back", GUILayout.MaxWidth (0.3f * width)))
					{
						Application.LoadLevel (1);
					}
					GUILayout.Space (0.3f * width);
					if(GUILayout.Button ("Start", GUILayout.MaxWidth (0.3f * width)))
					{
						RequestedLevel.RequestLevel (stages[selected].reqLevel);
						Application.LoadLevel (stages[selected].level);
					}
				}
				GUILayout.EndHorizontal ();
			}
			GUILayout.EndVertical ();
		}
		GUILayout.EndArea ();

		GUI.skin.box.wordWrap = old;
	}
}
