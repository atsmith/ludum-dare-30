﻿using UnityEngine;
using System;
using System.Collections;

public class HighScore {

	private float m_time;
	private int m_deaths;
	private int m_level;

	public HighScore(int level, float time, int deaths)
	{
		m_time = time;
		m_deaths = deaths;
		m_level = level;
	}

	public float time { get { return m_time; } }

	public int deaths { get { return m_deaths; } }

	public int level { get { return m_level; } }

	public bool LessThan(HighScore other)
	{
		if(deaths == -1 || time == -1)
		{
			return false;
		}
		return (time < other.time && deaths < other.deaths);
	}

	public static bool operator<(HighScore hs1, HighScore hs2)
	{
		return hs1.LessThan (hs2);
	}
	public static bool operator>(HighScore hs1, HighScore hs2)
	{
		return !hs1.LessThan (hs2);
	}
}
