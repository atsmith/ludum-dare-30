﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Camera))]
public class CameraBackground : MonoBehaviour {
	
	public float changeTimeSec = 0.5f;
	public float timeToCompleteSec = 1.0f;

	private Color backgroundColor = Color.white;
	private Camera theCamera = null;
	private bool isChangingColor = false;
	private float timeSinceLastChange = 0.0f;
	private float percentComplete = 0.0f;

	// Use this for initialization
	void Start () {
		theCamera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!isChangingColor)
		{
			timeSinceLastChange += Time.deltaTime;

			if(timeSinceLastChange > changeTimeSec)
			{
				isChangingColor = true;
				timeSinceLastChange = 0.0f;
			}

		}
		if(isChangingColor)
		{
			if(percentComplete == 0)
			{
				RandomColor ();
				while(backgroundColor.Equals (Color.white) || backgroundColor.Equals (Color.black))
				{
					RandomColor ();
				}
			}
			percentComplete += Time.deltaTime / timeToCompleteSec;
			Color newColor = Color.Lerp (theCamera.backgroundColor, backgroundColor, percentComplete);

			theCamera.backgroundColor = newColor;

			if(percentComplete > 1.0f)
			{
				percentComplete = 0;
				isChangingColor = false;
			}
		}
	}

	void RandomColor()
	{
		backgroundColor.r = Random.Range (0.0f, 1.0f);
		backgroundColor.g = Random.Range (0.0f, 1.0f);
		backgroundColor.b = Random.Range (0.0f,1.0f);
	}
}
