﻿using UnityEngine;
using System.Collections;

public class LoadLevelAdditive : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if(RequestedLevel.requestedLevel != Application.loadedLevel && RequestedLevel.requestedLevel >= 0)
		{
			LevelScore.SetLevel (RequestedLevel.requestedLevel);
			Application.LoadLevelAdditive (RequestedLevel.requestedLevel);
			RequestedLevel.RequestLevel(-1);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
