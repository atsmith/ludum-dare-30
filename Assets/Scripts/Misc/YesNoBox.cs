﻿using UnityEngine;
using System;
using System.Collections;

public class YesNoBox : MonoBehaviour {

	public static void CreateBox(Action<bool> callback, string title, string message, Vector2 size)
	{
		GameObject obj = new GameObject("YesNoBox");
		YesNoBox ynb = obj.AddComponent<YesNoBox>();
		ynb.callback = callback;
		ynb.title = title;
		ynb.message = message;
		ynb.size = ((!Vector2.zero.Equals (size))? size : ynb.size);

	}

	public Action<bool> callback=null;
	public string title = "Yes or no?";
	public string message = "Message goes here.";
	public Vector2 size = new Vector2(400, 300);
	
	private Rect window;
	private Rect messageBox;
	private Rect yesButton;
	private Rect noButton;

	private int width, height;

	private GUIContent titleContent;
	private GUIContent msgContent;

	private bool answered;
	private bool response;

	// Use this for initialization
	void Start () {
		titleContent = new GUIContent(title);
		msgContent = new GUIContent(message);
		answered = false;
		response = false;
		SetupMenu ();
	}

	void SetupMenu()
	{
		width = Screen.width;
		height = Screen.height;
		window = new Rect(0.5f * width - 0.5f * size.x,
		                  0.5f * height - 0.5f * size.y,
		                  size.x, size.y);
		messageBox = new Rect(0.5f * width - 0.5f * size.x + 0.05f*size.x,
		                      0,
		                      size.x-0.05f*size.x, 0);
		yesButton = new Rect(0.5f * width - 0.5f * size.x,
		                     0.5f * height + 0.5f * size.y - 30,
		                     0.3f * size.x,
		                     30);
		noButton = new Rect(0.5f * width + 0.5f * size.x - 0.3f * size.x,
		                    0.5f * height + 0.5f * size.y - 30,
		                    0.3f * size.x,
		                    30);
	}
	
	// Update is called once per frame
	void Update () {
		if(Screen.width != width || Screen.height != height)
		{
			SetupMenu ();
		}
		if(answered)
		{
			if(null != callback)
			{
				callback(response);
			}
			DestroyImmediate (transform.gameObject);
		}
		if(Input.GetButtonDown ("start"))
		{
			if(null != callback)
			{
				callback(false);
			}
			DestroyImmediate (transform.gameObject);
		}
	}

	void OnGUI()
	{
		GUI.depth = 0;
		GUI.Box (window, "");
		GUI.Box (window, "");
		GUI.Box (window, "");
		GUI.Box (window, "");
		GUI.Box (window, "");
		GUI.Box (window, "");
		GUI.Box (window, "");
		int oldFont = GUI.skin.box.fontSize;
		GUI.skin.box.fontSize = 18;
		GUI.Box (window, titleContent);
		Vector2 titleSize = GUI.skin.box.CalcSize (titleContent);
		GUI.skin.box.fontSize = oldFont;
		messageBox.y = 0.5f * height - 0.5f * size.y + titleSize.y;
		messageBox.height = size.y - titleSize.y - (0.25f * size.y);
		GUI.skin.label.wordWrap = true;
		GUI.Label (messageBox, msgContent);
		if(GUI.Button (yesButton, "Yes"))
		{
			answered = true;
			response = true;
		}
		if(GUI.Button (noButton, "No"))
		{
			answered = true;
			response = false;
		}
	}
}
