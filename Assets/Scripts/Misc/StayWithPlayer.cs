﻿using UnityEngine;
using System;
using System.Collections;

public class StayWithPlayer : MonoBehaviour {

	public GameObject player = null;
	public float maxHeight = 500.0f;
	public float minHeight = 0.0f;

	private Vector3 playerPosition;

	// Use this for initialization
	void Start () {
		if(null == player)
		{
			Debug.LogError ("Player cannot be null.");
			throw new NullReferenceException("player cannot be null.");
		}
		playerPosition = player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if(!playerPosition.Equals (player.transform.position))
		{
			playerPosition = player.transform.position;
			transform.position = new Vector3(transform.position.x, 
			                                 Mathf.Clamp (playerPosition.y, minHeight, maxHeight),
			                                 transform.position.z);
		}
	}
}
