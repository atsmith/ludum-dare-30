﻿using UnityEngine;
using System.Collections;

public class ChangeWorld : MonoBehaviour {

	private bool insidePlatform = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown ("world_change") && !insidePlatform)
		{
			if(AlternateWorlds.worldType == AlternateWorlds.CurrentWorld.BLACK)
			{
				AlternateWorlds.SetWorldType(AlternateWorlds.CurrentWorld.WHITE);
			}
			else
			{
				AlternateWorlds.SetWorldType(AlternateWorlds.CurrentWorld.BLACK);
			}
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag.Equals ("Platform"))
		{
			insidePlatform = true;
		}
	}

	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.tag.Equals ("Platform"))
		{
			insidePlatform = false;
		}
	}
}
