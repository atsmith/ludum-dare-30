﻿using UnityEngine;
using System.Collections;

public class Respawn : MonoBehaviour {
	
	private Vector3 startPos;
	private Quaternion startRot;
	private Quaternion camRot;
	private Camera cam;

	// Use this for initialization
	void Start () {
		cam = GetComponentInChildren<Camera>();
		camRot = cam.transform.localRotation;
		startPos = transform.position;
		startRot = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		if(GameState.isPaused)return;
		if(Input.GetButtonDown ("respawn"))
		{
			DeathNoPenalty ();
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag.Equals ("Death"))
		{
			Death();
		}
	}

	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.tag.Equals ("Death"))
		{
			LevelScore.DeathPenalty ();
		}
	}

	void Death()
	{
		DeathNoPenalty();
	}

	void DeathNoPenalty()
	{
		transform.position = startPos;
		transform.rotation = startRot;
		cam.transform.localRotation = camRot;
		rigidbody.velocity = Vector3.zero;
	}
}
