﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {
	
	public Camera playerCamera = null;
	public float rotationSpeed = 10.0f;
	public float mouseSensitivityX = 0.3f;
	public float mouseSensitivityY = 0.3f;
	
	private float maxAngleY = 70.0f;
	private float minAngleY;

	private float rotationY = 0.0f;

	private Quaternion originalYRotation;

	// Use this for initialization
	void Start () {
		minAngleY = - maxAngleY;
		Screen.lockCursor = true;
		mouseSensitivityX = Mathf.Clamp (mouseSensitivityX, 0, 1);
		mouseSensitivityY = Mathf.Clamp (mouseSensitivityY, 0, 1);
		if(null == playerCamera)
		{
			GameObject obj = transform.GetChild (1).gameObject;
			playerCamera = obj.GetComponent<Camera>();
		}

		originalYRotation = playerCamera.transform.localRotation;
	}
	
	// Update is called once per frame
	void Update () {
		if(GameState.isPaused)return;
		Screen.lockCursor = true;

		float rotationX = Input.GetAxis ("Mouse X") * mouseSensitivityX;
		rotationY += Input.GetAxis ("Mouse Y") * mouseSensitivityY * ((Settings.isInverted)?-1:1) * rotationSpeed;

		rotationY = Mathf.Clamp (rotationY, minAngleY, maxAngleY);

		Quaternion yQuaternion = Quaternion.AngleAxis (rotationY, Vector3.right);
		playerCamera.transform.localRotation = originalYRotation * yQuaternion;


		transform.RotateAround (transform.position, 
		                        transform.up, 
		                        rotationX * rotationSpeed);



	}
}
