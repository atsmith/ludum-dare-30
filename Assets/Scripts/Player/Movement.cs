﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody))]
[RequireComponent (typeof(Collider))]
public class Movement : MonoBehaviour {

	//public float maxRunSpeed = 20.0f;
	public float maxSpeed = 15.0f;
	public float maxStrafeSpeed = 10.0f;
	public float jumpSpeed = 30.0f;

	Rigidbody rigid;
	bool isJumping = true;

	private Vector3 pausedVelocity;
	private Vector3 pausedPosition;

	// Use this for initialization
	void Start () {
		rigid = gameObject.GetComponent<Rigidbody>();
		pausedVelocity = Vector3.zero;
	}

	void FixedUpdate()
	{
		if(GameState.isPaused)
		{
			pausedVelocity = rigid.velocity;
			rigid.velocity = Vector3.zero;
			transform.position = pausedPosition;
			return;
		}
		else if(!Vector3.zero.Equals (pausedVelocity))
		{
			rigid.velocity = pausedVelocity;
			pausedVelocity = Vector3.zero;
		}
		// begin jump stuff
		if(Input.GetButton ("jump")&& !isJumping && jumpSpeed != 0)
		{
			rigid.velocity = new Vector3(rigid.velocity.x, jumpSpeed, rigid.velocity.z);
			isJumping = true;
			LevelScore.jumped ();
		}

		float maxPossibleVel = (maxSpeed + maxStrafeSpeed) / 2.0f;
		
		Vector2 forwardMotion;
		Vector2 sidewaysMotion;
		
		if(Input.GetAxis ("move") != 0 && !isJumping)
		{
			forwardMotion = new Vector2(transform.forward.x, transform.forward.z) * Input.GetAxis ("move") * maxSpeed;
			
		}
		else
		{
			forwardMotion = Vector2.zero;
		}
		
		if(Input.GetAxis ("strafe") != 0 && !isJumping)
		{
			sidewaysMotion = new Vector2(transform.right.x, transform.right.z) * Input.GetAxis ("strafe") * maxStrafeSpeed;
		}
		else
		{
			sidewaysMotion = Vector2.zero;
		}
		Vector2 final;
		
		if(!forwardMotion.Equals (Vector2.zero) && !sidewaysMotion.Equals (Vector2.zero))
		{
			final = Vector2.Lerp (sidewaysMotion, forwardMotion, 0.5f);
		}
		else if(sidewaysMotion.Equals (Vector2.zero))
		{
			final = forwardMotion;
		}
		else if(forwardMotion.Equals (Vector2.zero))
		{
			final = sidewaysMotion;
		}
		else
		{
			final = Vector2.zero;
		}
		if(final.magnitude > maxPossibleVel)
		{
			final.Normalize ();
			final *= maxPossibleVel;
		}
		if(!final.Equals (Vector2.zero))
		{
			rigid.velocity = new Vector3(final.x, rigid.velocity.y, final.y);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(!GameState.isPaused)
		{
			pausedPosition = transform.position;
		}

	}

	void OnCollisionEnter(Collision col)
	{
		string tag = col.gameObject.tag;
		if(tag.Equals ("Ground") || tag.Equals ("Platform"))
		{
			isJumping = false;
		}
	}

	void OnCollisionStay(Collision col)
	{
		if(col.gameObject.tag.Equals ("Platform"))
		{
			transform.parent = col.transform;
		}
		else
		{
			transform.parent = null;
		}
	}

	void OnCollisionExit(Collision col)
	{
		string tag = col.gameObject.tag;
//		if(tag.Equals ("Ground") || tag.Equals ("Platform"))
//		{
//			isJumping = true;
//		}
		if(tag.Equals ("Platform"))
		{
			transform.parent = null;
		}
	}
}
