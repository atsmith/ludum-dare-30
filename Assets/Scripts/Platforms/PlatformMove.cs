using UnityEngine;
using System.Collections;
using System;

public class PlatformMove : MonoBehaviour {

	public enum MovementDirection
	{
		HorizontalX,
		HorizontalZ,
		Vertical,
		HorAndVerXY,
		HorAndVerZY
	}

	public MovementDirection movementDirection = MovementDirection.Vertical;
	public Vector2 movementSpeed = new Vector2(5.0f, 5.0f);
	public Vector2 maximumDistanceChange = new Vector2(10.0f, 10.0f);

	private Vector3 startingPosition;
	private Vector3 curPos;
	private Vector2 currentVelocity;
	private Vector2 timeToSwitch;
	private Vector2 lastSwitch = Vector2.zero;
	private bool firstSwitch = false;

	// Use this for initialization
	void Start () {
		currentVelocity = new Vector2(-1,-1);
		startingPosition = transform.localPosition;
		if(movementSpeed.y != 0)
		{
			timeToSwitch.y = maximumDistanceChange.y / maximumDistanceChange.y;
		}
		else
		{
			throw new DivideByZeroException("movementSpeed.y cannot be 0.");
		}
		if(movementSpeed.x != 0)
		{
			timeToSwitch.x = maximumDistanceChange.x / maximumDistanceChange.y;
		}
		else
		{
			throw new DivideByZeroException("movementSpeed.x cannot be 0.");
		}
	}

	bool SwitchableY()
	{
		if(lastSwitch.y >= timeToSwitch.y)
		{
			return true;
		}

		return false;
	}
	bool SwitchableX()
	{
		if(lastSwitch.x >= timeToSwitch.x)
		{
			return true;
		}

		return true;
	}
	
	// Update is called once per frame
	void Update () {
		if(GameState.isPaused)return;
		curPos = transform.localPosition;
		Vector3 posDiff = Vector3.zero;
		lastSwitch.x += Time.deltaTime;
		lastSwitch.y += Time.deltaTime;
		bool switched = false;
		switch(movementDirection)
		{
		case MovementDirection.Vertical:
			if(Distance () >= maximumDistanceChange.y && SwitchableY ())
			{
				currentVelocity.y *= -1;
				lastSwitch = Vector2.zero;
				switched = true;
			}
			{
				posDiff.y = MoveY ();
			}
			break;
		case MovementDirection.HorizontalX:
			if(Distance () >= maximumDistanceChange.x && SwitchableX ())
			{
				currentVelocity.x *= -1;
				lastSwitch = Vector2.zero;
				switched = true;
			}
			{
				posDiff.x = MoveX ();
			}
			break;
		case MovementDirection.HorizontalZ:
			if(Distance () >= maximumDistanceChange.x && SwitchableX ())
			{
				currentVelocity.x *= -1;
				lastSwitch = Vector2.zero;
				switched = true;
			}
			{
				posDiff.z = MoveZ ();
			}
			break;
		case MovementDirection.HorAndVerXY:
			if(Distance () >= maximumDistanceChange.magnitude && SwitchableX () && SwitchableY ())
			{
				currentVelocity.x *= -1;
				currentVelocity.y *= -1;
				lastSwitch = Vector2.zero;
				switched = true;
			}
			{
				posDiff.x = MoveX ();
				posDiff.y = MoveY ();
			}
			break;
		case MovementDirection.HorAndVerZY:
			if(Distance () >= maximumDistanceChange.magnitude && SwitchableX () && SwitchableY ())
			{
				currentVelocity.x *= -1;
				currentVelocity.y *= -1;
				lastSwitch = Vector2.zero;
				switched = true;
			}
			{
				posDiff.y = MoveY ();
				posDiff.z = MoveZ ();
			}
			break;
		}

		if(!firstSwitch && switched)
		{
			timeToSwitch.x = 2 * maximumDistanceChange.x / movementSpeed.x;
			timeToSwitch.y = 2 * maximumDistanceChange.y / movementSpeed.y;
			firstSwitch = true;
		}

		transform.localPosition = new Vector3(curPos.x + posDiff.x,
		                                      curPos.y + posDiff.y,
		                                      curPos.z + posDiff.z);
	}

	float Distance()
	{
		return Mathf.Abs (Vector3.Distance (startingPosition, curPos));
	}

	float MoveX()
	{
		return currentVelocity.x * movementSpeed.x * Time.deltaTime;
	}

	float MoveY()
	{
		return currentVelocity.y * movementSpeed.y * Time.deltaTime;
	}

	float MoveZ()
	{
		return currentVelocity.x * movementSpeed.x * Time.deltaTime;
	}
}
