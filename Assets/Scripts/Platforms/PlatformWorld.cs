﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Collider))]
[RequireComponent (typeof(MeshRenderer))]
public class PlatformWorld : MonoBehaviour {

	public AlternateWorlds.CurrentWorld solidInWorld = AlternateWorlds.CurrentWorld.BLACK;
	public float alphaTransparency = 0.3f;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(AlternateWorlds.worldType != solidInWorld)
		{
			collider.isTrigger = true;
			renderer.material.SetFloat ("_Alpha", alphaTransparency);
		}
		else
		{
			collider.isTrigger = false;
			renderer.material.SetFloat ("_Alpha", 1);
		}
	}
}
