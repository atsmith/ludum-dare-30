﻿using UnityEngine;
using System.Collections;

public class EscapeMenuGUI : MonoBehaviour {

	public int sizeX, sizeY;

	private Rect location;
	private int width, height;

	private bool visible = false;
	private Vector2 scrollPos = Vector2.zero;
	private int buttonCount = 3;
	private bool guiEnabled = true;
	// Use this for initialization
	void Start () {
	
		SetupMenu ();
	}

	void QuitToMainMenu(bool response)
	{
		if(response)
		{
			Application.LoadLevel (1);
		}
		guiEnabled = true;
	}

	void QuitToDesktop(bool response)
	{
		if(response)
		{
			Application.Quit ();
		}
		guiEnabled = true;
	}



	void SetupMenu()
	{
		width = Screen.width;
		height = Screen.height;

		location = new Rect(0.5f * width - 0.5f * sizeX,
		                    0.5f * height - 0.5f * sizeY,
		                    sizeX,
		                    sizeY);
	}
	
	// Update is called once per frame
	void Update () {
		if(Screen.width != width || Screen.height != height)
		{
			SetupMenu ();
		}
		if(Input.GetButtonDown ("start") && guiEnabled)
		{
			Toggle();
		}
		else if(Input.GetButtonDown ("start") && !guiEnabled)
		{
			guiEnabled = true;
		}
		if(visible && !GameState.isPaused)
		{
			GameState.TogglePause ();
		}
		else if(!visible && GameState.isPaused)
		{
			GameState.TogglePause ();
		}
		if(visible)
		{
			Screen.lockCursor = false;
		}
	}

	void Toggle()
	{
		visible = !visible;
		GameState.TogglePause();
	}

	void OnGUI()
	{
		if(!visible) return;
		GUI.Box (location, "");
		GUI.Box (location, "");
		GUI.Box (location, "");
		GUI.enabled = guiEnabled;
		GUILayout.BeginArea (location);
		{
			scrollPos = GUILayout.BeginScrollView (scrollPos);
			{
				GUILayout.BeginVertical ();
				{
					if(GUILayout.Button ("Resume", GUILayout.MaxHeight (sizeY / buttonCount), GUILayout.ExpandHeight (true)))
					{
						Toggle ();
					}
					if(GUILayout.Button ("Quit to Main Menu", GUILayout.MaxHeight (sizeY / buttonCount), GUILayout.ExpandHeight (true)))
					{
						guiEnabled = false;
						YesNoBox.CreateBox(QuitToMainMenu, "You Sure?", "You will lose any unsaved data.", new Vector2(200, 100));
					}

					if(GUILayout.Button ("Quit to Desktop", GUILayout.MaxHeight (sizeY / buttonCount), GUILayout.ExpandHeight (true)))
					{
						guiEnabled = false;
						YesNoBox.CreateBox(QuitToMainMenu, "You Sure?", "You will lose any unsaved data.", new Vector2(200, 100));
					}
				}
				GUILayout.EndVertical ();
			}
			GUILayout.EndScrollView ();
		}
		GUILayout.EndArea ();
		GUI.enabled = true;
	}
}
