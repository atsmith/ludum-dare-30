﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent (typeof(GUIText))]
public class TimePassed : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float span = LevelScore.levelTime;
		guiText.text = span + " Seconds";
		LevelScore.tick ();
	}
}
