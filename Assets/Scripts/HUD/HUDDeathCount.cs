﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(GUIText))]
public class HUDDeathCount : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		guiText.text = LevelScore.deathCount + " Deaths";
	}
}
