﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(MeshRenderer))]
public class GroundColor : MonoBehaviour {

	public Color blackUniverse = new Color(0.8f, 0.8f, 0.8f);
	public Color blackUniverseEmissivity = new Color(1,1,1);
	public Color whiteUniverse = new Color(0.2f, 0.2f, 0.2f);
	public Color whiteUniverseEmissivity = new Color(0,0,0);

	AlternateWorlds.CurrentWorld lastChecked;

	// Use this for initialization
	void Start () {
		lastChecked = AlternateWorlds.worldType;
	}
	
	// Update is called once per frame
	void Update () {
		if(AlternateWorlds.worldType != lastChecked)
		{
			Color nC, nE;
			lastChecked = AlternateWorlds.worldType;
			if(AlternateWorlds.CurrentWorld.BLACK == lastChecked)
			{
				nC = blackUniverse;
				nE = blackUniverseEmissivity;
			}
			else
			{
				nC = whiteUniverse;
				nE = whiteUniverseEmissivity;
			}
			renderer.material.SetColor ("_MainColor", nC);
			renderer.material.SetColor ("_Emission", nE);
		}
	}
}
