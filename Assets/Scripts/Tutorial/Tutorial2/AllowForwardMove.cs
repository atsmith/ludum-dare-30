﻿using UnityEngine;
using System;
using System.Collections;

public class AllowForwardMove : MonoBehaviour {

	public GUIText notification = null;

	// Use this for initialization
	void Start () {
		if(null == notification)
		{
			Debug.LogError ("notification cannot be null.");
			throw new NullReferenceException("Notification cannot be null.");
		}
		notification.transform.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.tag.Equals ("Player"))
		{
			Movement m = col.gameObject.GetComponent<Movement>();
			m.maxSpeed = 6;
			m.maxStrafeSpeed = 4;
			notification.transform.gameObject.SetActive(true);
			notification.enabled = true;
			Animator anim = notification.gameObject.GetComponent<Animator>();
			anim.enabled = true;
		}
	}
}
