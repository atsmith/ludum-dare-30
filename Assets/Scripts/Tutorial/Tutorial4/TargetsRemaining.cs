﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(GUIText))]
public class TargetsRemaining : MonoBehaviour {

	private static int targetsRemaining = 0;

	public static void AddTarget()
	{
		targetsRemaining++;
	}

	public static void RemoveTarget()
	{
		targetsRemaining--;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		guiText.text = targetsRemaining + " Targets Remain";
	}
}
