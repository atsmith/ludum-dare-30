﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {

	// Use this for initialization
	void Start () {
		TargetsRemaining.AddTarget ();
	}

	void OnDestroy()
	{
		TargetsRemaining.RemoveTarget ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
