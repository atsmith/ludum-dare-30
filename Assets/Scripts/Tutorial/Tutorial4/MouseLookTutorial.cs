﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MouseLookTutorial : MonoBehaviour {

	public Camera playerCamera = null;
	public GameObject warp = null;
	public int levelToLoad = 0;
	public int requestedLevel = -1;
	public RuntimeAnimatorController control = null;
	public GameObject[] destroyIfExist = null;

	private List<GameObject> tutorialTargets;
	private bool warpCreated = false;

	// Use this for initialization
	void Start () {
		tutorialTargets = new List<GameObject>();
		GameObject [] allObjects = GameObject.FindGameObjectsWithTag("TutorialTarget");
		foreach(GameObject obj in allObjects)
		{
			if(obj.activeInHierarchy)
			{
				tutorialTargets.Add (obj);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(null != playerCamera)
		{
			RaycastHit camHit;
			if(Physics.Raycast (playerCamera.transform.position, playerCamera.transform.forward, out camHit))
			{
				if(camHit.transform.name.Contains ("TutorialTargetY"))
				{
					tutorialTargets.Remove(camHit.transform.gameObject);
					Destroy (camHit.transform.gameObject);
				}
			}
		}
		RaycastHit bodyHit;
		if(Physics.Raycast (transform.position, transform.forward, out bodyHit))
		{
			if(bodyHit.transform.name.Contains("TutorialTargetX"))
			{
				tutorialTargets.Remove (bodyHit.transform.gameObject);
				Destroy (bodyHit.transform.gameObject);
			}
		}

		if(tutorialTargets.Count == 0 && null != warp && !warpCreated)
		{
			foreach(GameObject obj in destroyIfExist)
			{
				Destroy (obj);
			}
			Vector3 pos = new Vector3(15.0f, 1.66f, 0.0f);
			GameObject nWarp = (GameObject)Instantiate (warp, pos, Quaternion.identity);
			WarpToMap wtm = nWarp.GetComponent<WarpToMap>();
			wtm.levelToLoad = levelToLoad;
			wtm.levelToRequest = requestedLevel;
			warpCreated = true;

			Movement mv = GetComponent<Movement>();
			mv.maxSpeed = 6;
			mv.maxStrafeSpeed = 4;
			mv.jumpSpeed = 7;

			GameObject notification = new GameObject("Notification");
			GUIText text = notification.AddComponent<GUIText>();
			Animator anim = notification.AddComponent<Animator>();
			notification.AddComponent<AnimationHasEnded>();
			notification.transform.position = new Vector3(0.05f, 1.3f, 0.0f);
			text.text = "Movement enabled. Warp unlocked.";
			text.fontSize = 26;
			text.alignment = TextAlignment.Left;
			text.anchor = TextAnchor.MiddleLeft;
			anim.applyRootMotion = true;
			anim.runtimeAnimatorController = control;
			anim.Play ("DropDownAndUp");
		}
	}
}
