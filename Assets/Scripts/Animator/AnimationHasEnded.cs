﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Animator))]
public class AnimationHasEnded : MonoBehaviour {

	public Animator animator;

	private Animator anim;
	private bool done = false;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!anim.GetCurrentAnimatorStateInfo (0).IsName ("DropDownAndUp") && !done)
		{
			if(null != animator)
			{
				animator.enabled = true;
				animator.Play("DropDownAndUp");
			}
			done = true;
			Destroy (transform.gameObject);
		}
	}
}
