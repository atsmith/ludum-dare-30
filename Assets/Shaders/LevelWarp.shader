﻿Shader "Custom/LevelWarp" {
	Properties {
		_Radius ("Radius", Float) = 0.3
		_Width ("Width", Float) = 1
		_Height ("Height", Float) = 1
		_CenterX("Center X", Float) = 0.5
		_CenterY("Center Y", Float) = 0.5
		_Angle("Angle", Float) = 0.8
		_MainTex("Main Texture", 2D) = "white" {}
	}
	
	SubShader {
		Tags
		{
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
		}
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			uniform float _Radius;
			uniform float _Width;
			uniform float _Height;
			uniform float _CenterX;
			uniform float _CenterY;
			uniform float _Angle;
			
			
			struct vertexInput
			{
				float4 pos : POSITION;
				float4 texcoord0 : TEXCOORD0;
			};
			
			struct fragmentInput
			{
				float4 position : SV_POSITION;
				float4 texcoord0 : TEXCOORD0;
			};
						
			fragmentInput vert(vertexInput i)
			{
				fragmentInput o;
				o.position = mul(UNITY_MATRIX_MVP, i.pos);
				o.texcoord0 = i.texcoord0;
				return o;
			}
			
			float4 frag(fragmentInput i) : COLOR
			{
				float2 texSize = float2(_Width, _Height);
				float2 uv = float2(i.texcoord0.xy);
				float2 tc = (uv * texSize);
				tc -= float2(_CenterX, _CenterY);
				float dist = length(tc);
				if(dist < _Radius)
				{
					float percent = (_Radius - dist) / _Radius;
					float theta = percent * percent * _Angle * 8.0;
					float s = sin(theta);
					float c = cos(theta);
					tc = float2(dot(tc, float2(c, -s)), dot(tc, float2(s, c)));
				}
				tc += float2(_CenterX, _CenterY);
				
				float3 col = tex2D(_MainTex, tc / texSize).rgb;
				return float4(col, 1);
			}
			
			ENDCG
		}
	}
	FallBack "Diffuse"
}
