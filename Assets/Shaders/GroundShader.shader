﻿Shader "Custom/GroundShader" {
	Properties {
		_MainColor("Main Color", Color) = (1,1,1,1)
		_Emission("Emissivity", Color) = (0,0,0,0)
	}
	SubShader {
		Pass
		{
			Material
			{
				Ambient[_MainColor]
				Emission[_Emission]
			}
			Lighting On
		}
	} 
	FallBack "Diffuse"
}
