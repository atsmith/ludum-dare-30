﻿Shader "Custom/IHasNoIdeaWhatImDoing" {
	Properties {
		_Red("Red", Range(0, 1)) = 0.5
		_Green("Green", Range(0,1)) = 0.5
		_Blue("Blue", Range(0,1)) = 0.5
		_Alpha("Visibility", float) = 1
	}
	SubShader {
		Tags
		{
			"Queue"="Transparent"
			"RenderType"="Transparent"
		}
		
		Pass
		{
			ZWrite On
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
	
			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 color : COLOR0;
			};
			
			uniform float _Alpha;
			uniform float _Red;
			uniform float _Green;
			uniform float _Blue;
	
			v2f vert(appdata_base v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.color.rgb = float3(_Red, _Green, _Blue);
				return o;
			}
			
			float4 frag(v2f i) : COLOR
			{
				return float4(i.color, clamp(_Alpha, 0, 1));
			}
			
			ENDCG
			
			Lighting On
		}
		
	}
	FallBack "Diffuse"
}
